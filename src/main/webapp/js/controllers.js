/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module("Controllers", []).
        controller("UserController", ['$scope', '$http', function ($scope, $http) {
                $scope.email = '';
                $scope.password = '';
                $scope.registerFields = {
                    name: '',
                    email: '',
                    password: ''
                };

                $scope.register = function () {
                    $http.post('rest/user/register', $scope.registerFields).
                            success(function (data, status, headers, config) {
                                console.info(data.msg);
                            }).
                            error(function (data, status, headers, config) {
                                console.error(data.error);
                            });
                };


                $scope.login = function () {
                    $http.post('rest/user/login', {email: $scope.email, password: $scope.password}).
                            success(function (data, status, headers, config) {
                                localStorage.setItem("token", data.token);
//                                $rootScope.authToken = data.token;
                            }).
                            error(function (data, status, headers, config) {
                                console.error(data.error);
                            });
                };

                $scope.test = function () {
                    $http.defaults.headers.common.AuthToken = localStorage.getItem("token");
                    $http.get('rest/user/test').
                            success(function (data, status, headers, config) {
                                console.info('AuthToken ok');
                            }).
                            error(function (data, status, headers, config) {
                                console.error(data.error);
                            });
                };
            }]).
        controller('CatchController', ['$scope', '$http', function ($scope, $http) {
                $scope.newCatch = {
                    species: {id: 0},
                    weight: 0,
                    length: 0,
                    catchTime: new Date(),
                    longitude: 0,
                    latitude: 0,
                    location: '',
                    image: '',
                    lure: '',
                    technique: '',
                    depth: 0,
                    weather: '',
                    airTemperature: 0,
                    waterTemperature: 0
                };

                $scope.add = function () {
                    $http.defaults.headers.common.AuthToken = localStorage.getItem('token');
                    $http.put('rest/catch', $scope.newCatch).
                            success(function (data, status, headers, config) {
                                console.info(data.msg);
                            }).
                            error(function (data, status, headers, config) {
                                console.error(data.error);
                            });
                };
            }]);
