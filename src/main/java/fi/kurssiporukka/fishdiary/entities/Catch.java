/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author koulutus
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Catch.findAll", query = "SELECT c FROM Catch c"),
    @NamedQuery(name = "Catch.findById", query = "SELECT c FROM Catch c WHERE c.id = :id"),
    @NamedQuery(name = "Catch.findByWeigth", query = "SELECT c FROM Catch c WHERE c.weight = :weight"),
    @NamedQuery(name = "Catch.findByLength", query = "SELECT c FROM Catch c WHERE c.length = :length"),
    @NamedQuery(name = "Catch.findByCatchTime", query = "SELECT c FROM Catch c WHERE c.catchTime = :catchTime"),
    @NamedQuery(name = "Catch.findByLongitude", query = "SELECT c FROM Catch c WHERE c.longitude = :longitude"),
    @NamedQuery(name = "Catch.findByLatitude", query = "SELECT c FROM Catch c WHERE c.latitude = :latitude"),
    @NamedQuery(name = "Catch.findByLure", query = "SELECT c FROM Catch c WHERE c.lure = :lure"),
    @NamedQuery(name = "Catch.findByTechnique", query = "SELECT c FROM Catch c WHERE c.technique = :technique"),
    @NamedQuery(name = "Catch.findByDepth", query = "SELECT c FROM Catch c WHERE c.depth = :depth"),
    @NamedQuery(name = "Catch.findByWeather", query = "SELECT c FROM Catch c WHERE c.weather = :weather"),
    @NamedQuery(name = "Catch.findByAirTemperature", query = "SELECT c FROM Catch c WHERE c.airTemperature = :airTemperature"),
    @NamedQuery(name = "Catch.findByWaterTemperature", query = "SELECT c FROM Catch c WHERE c.waterTemperature = :waterTemperature")})
public class Catch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Species species;
    private Double weight;
    private Double length;
    @Temporal(TemporalType.TIMESTAMP)
    private Date catchTime;
    private Double longitude;
    private Double latitude;
    private String location;
    @Lob
    private byte[] image;
    private String lure;
    private String technique;
    private Double depth;
    private String weather;
    private Double airTemperature;
    private Double waterTemperature;
    @ManyToOne(optional = false)
    private User user;

    public Catch() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Date getCatchTime() {
        return catchTime;
    }

    public void setCatchTime(Date catchTime) {
        this.catchTime = catchTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getLure() {
        return lure;
    }

    public void setLure(String lure) {
        this.lure = lure;
    }

    public String getTechnique() {
        return technique;
    }

    public void setTechnique(String technique) {
        this.technique = technique;
    }

    public Double getDepth() {
        return depth;
    }

    public void setDepth(Double depth) {
        this.depth = depth;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public Double getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(Double airTemperature) {
        this.airTemperature = airTemperature;
    }

    public Double getWaterTemperature() {
        return waterTemperature;
    }

    public void setWaterTemperature(Double waterTemperature) {
        this.waterTemperature = waterTemperature;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catch)) {
            return false;
        }
        Catch other = (Catch) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.kurssiporukka.fishdiary.entities.Catch[ id=" + id + " ]";
    }

}
