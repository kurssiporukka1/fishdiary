/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

/**
 *
 * @author ipe
 */
@Startup
@Singleton(name = "ApplicationSettings")
public class ApplicationSettings {

    private byte[] key;

    public ApplicationSettings() {
    }

    @PostConstruct
    public void init() {
        this.key = "Esimerkkiavain".getBytes();
    }

    @Produces
    @Key
    public byte[] getKey() {
        return this.key;
    }

}
