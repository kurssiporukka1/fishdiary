/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.facades;

import fi.kurssiporukka.fishdiary.entities.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 *
 * @author koulutus
 */
@Stateless
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext(unitName = "FishDiaryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    public User createNewUser(User user) {
        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
        user.setPassword(encryptor.encryptPassword(user.getPassword()));
        create(user);
        return user;

    }

    public boolean userExist(User user) {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE u.email = :email OR u.name = :name", User.class);
        query.setParameter("email", user.getEmail()).setParameter("name", user.getName());
        return !query.getResultList().isEmpty();

    }

    public User findByEmailAndPassword(User user) {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class);
        query.setParameter("email", user.getEmail());
        try {
            User managedUser = query.getSingleResult();
            StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
            boolean passwordMatch = encryptor.checkPassword(user.getPassword(), managedUser.getPassword());
            if (!passwordMatch) {
                return null;
            } else {
                return managedUser;
            }
        } catch (NoResultException e) {
            return null;
        }

    }

}
