/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.facades;

import fi.kurssiporukka.fishdiary.entities.Catch;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author koulutus
 */
@Stateless
public class CatchFacade extends AbstractFacade<Catch> {
    @PersistenceContext(unitName = "FishDiaryPU")
    private EntityManager em;
    @Inject UserFacade userFacade;
    @Inject SpeciesFacade speciesFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CatchFacade() {
        super(Catch.class);
    }
    
    public void create(long userId, Catch c) {
        c.setUser(userFacade.find(userId));
        c.setSpecies(speciesFacade.find(c.getSpecies().getId()));
        create(c);
    }
    
}
