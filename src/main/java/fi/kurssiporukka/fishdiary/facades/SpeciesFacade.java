/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.facades;

import fi.kurssiporukka.fishdiary.entities.Species;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author koulutus
 */
@Stateless
public class SpeciesFacade extends AbstractFacade<Species> {
    @PersistenceContext(unitName = "FishDiaryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SpeciesFacade() {
        super(Species.class);
    }
    
}
