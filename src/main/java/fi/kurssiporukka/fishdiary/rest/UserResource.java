/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.rest;

import fi.kurssiporukka.fishdiary.Key;
import fi.kurssiporukka.fishdiary.entities.User;
import fi.kurssiporukka.fishdiary.facades.UserFacade;
import fi.kurssiporukka.fishdiary.rest.Authenticated;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.Instant;
import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author koulutus
 */
@Stateless
@Path("user")
public class UserResource {

    @Inject
    private UserFacade userFacade;
    @Context
    private UriInfo context;
    @Inject
    @Key
    private byte[] key;

    @Path("/register")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(User user) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        if (userFacade.userExist(user)) {
            jsonObjectBuilder.add("error", "Sähköposti tai nimi ei kelpaa");
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(jsonObjectBuilder.build()).build();
        } else {
            userFacade.createNewUser(user);
            jsonObjectBuilder.add("msg", "Käyttäjätili luotu");
            return Response.created(null).entity(jsonObjectBuilder.build()).build();
        }
    }

    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(User user) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        user = userFacade.findByEmailAndPassword(user);
        if (user != null) {
            String token = Jwts.builder().setIssuedAt(Date.from(Instant.now())).setIssuer("FishDiary").setSubject(user.getId().toString()).signWith(SignatureAlgorithm.HS256, key).compact();
            jsonObjectBuilder.add("token", token);
            return Response.ok(jsonObjectBuilder.build()).build();
        } else {
            jsonObjectBuilder.add("error", "Väärä käyttäjätunnus tai salasana");
            return Response.status(Response.Status.UNAUTHORIZED).entity(jsonObjectBuilder.build()).build();
        }
    }

    @Authenticated
    @Path("/test")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response test(@HeaderParam("AuthToken") String token) {
        return Response.ok().build();
    }

}
