/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.rest;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author koulutus
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(fi.kurssiporukka.fishdiary.rest.AuthenticationFilter.class);
        resources.add(fi.kurssiporukka.fishdiary.rest.CatchResource.class);
        resources.add(fi.kurssiporukka.fishdiary.rest.SpeciesResource.class);
        resources.add(fi.kurssiporukka.fishdiary.rest.UserResource.class);
    }
    
}
