/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.rest;

import fi.kurssiporukka.fishdiary.entities.Species;
import fi.kurssiporukka.fishdiary.facades.SpeciesFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author koulutus
 */
@Stateless
@Path("species")
public class SpeciesResource {

    @Context
    private UriInfo context;
    @Inject
    private SpeciesFacade speciesFacade;

    /**
     * Creates a new instance of SpeciesResource
     */
    public SpeciesResource() {
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Species> getAll() {
        return speciesFacade.findAll();
    }
}
