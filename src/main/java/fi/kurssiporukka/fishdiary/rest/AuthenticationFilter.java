/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.rest;

import fi.kurssiporukka.fishdiary.ApplicationSettings;
import fi.kurssiporukka.fishdiary.Key;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import javax.annotation.ManagedBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author koulutus
 */
@Provider
@Authenticated
public class AuthenticationFilter implements ContainerRequestFilter {

    private byte[] key;
//    @Inject @Key byte[] key;
    


    public AuthenticationFilter() throws NamingException {
        InitialContext context = new InitialContext();
        ApplicationSettings as = (ApplicationSettings) context.lookup("java:global/FishDiary/ApplicationSettings");
        this.key = as.getKey();
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        String token = requestContext.getHeaders().getFirst("AuthToken");
        System.out.println("Token: " + token);
        if (token == null || token.equals("null")) {
            jsonObjectBuilder.add("error", "No authentication token");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(jsonObjectBuilder.build()).build());
            return;
        }
        try {
            Jwt parsed = Jwts.parser().setSigningKey(key).parse(token);
            Claims body = (Claims) parsed.getBody();
            String id = body.getSubject();
            requestContext.getHeaders().add("subject", id);
        } catch (SignatureException e) {
            jsonObjectBuilder.add("error", "Token signature failure");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(jsonObjectBuilder.build()).build());
        }
    }

}
