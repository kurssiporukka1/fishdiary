/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.kurssiporukka.fishdiary.rest;

import fi.kurssiporukka.fishdiary.entities.Catch;
import fi.kurssiporukka.fishdiary.facades.CatchFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author koulutus
 */
@Stateless
@Path("catch")
public class CatchResource {

    @Inject
    CatchFacade catchFacade;
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CatchResource
     */
    public CatchResource() {
    }

    @Authenticated
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(@HeaderParam("subject") long subject, Catch c) {
        catchFacade.create(subject, c);
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Catch> getAll(@QueryParam("first") int first, @QueryParam("max") int max) {
        if (first == 0 || max == 0) {
            return catchFacade.findAll();
        }
        else {
            return catchFacade.findRange(new int[]{first, max});
        }
    }

}
